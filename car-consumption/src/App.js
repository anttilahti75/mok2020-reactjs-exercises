import React, { useEffect, useState } from 'react';
import { Provider, useSelector, useDispatch } from "react-redux";
import { createStore } from "redux";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import firebase from 'firebase/app';
import "firebase/firestore";
import { LineChart, Line } from 'recharts';

import './App.css';

const firebaseConfig = {
  apiKey: process.env.REACT_APP_API_KEY,
  authDomain: process.env.REACT_APP_AUTH_DOMAIN,
  databaseURL: process.env.REACT_APP_DATABASE_URL,
  projectId: "demos-5a02e",
  storageBucket: process.env.REACT_APP_STORAGE_BUCKET,
  messagingSenderId: process.env.REACT_APP_MESSAGING_SENDER_ID,
  appId: process.env.REACT_APP_APP_ID
};

// Initialize Firebase
firebase.initializeApp(firebaseConfig);

const fuelCost = (liters, literPrice) => {
  return liters * literPrice
}

const consumptionPer100km = (km, liters) => {
  let hundredKms = parseFloat(km) / 100
  return (parseFloat(liters) / hundredKms).toFixed(2)
}

// Reducer actions

function addRefuelEvent(refuelingEvent) {
  return { type: "ADD_REFUELING_EVENT", refuelingEvent: refuelingEvent }
}

function deleteRefuelEvent(id) {
  return { type: "DELETE_REFUELING_EVENT", id: id}
}

function addDbData(arr) {
  return { type: "ADD_DATABASE_DATA", arr: arr}
}

function reducer(state = { refuelingEvents: [] }, action) {
  let newEvents = []
  switch (action.type) {
    case "ADD_REFUELING_EVENT":
      return {
        ...state,
        refuelingEvents: [...state.refuelingEvents, action.refuelingEvent]
      };
    case "DELETE_REFUELING_EVENT":
      newEvents = state.refuelingEvents.filter(item => item.id !== action.id)
      return {
        ...state,
        refuelingEvents: newEvents
      };
    case "ADD_DATABASE_DATA":
      for (let item of action.arr) {
        newEvents.push(item)
      }
      return {
        ...state,
        refuelingEvents: newEvents
      };
    default:
      return state;
  }
}

function RefuelEvent(props) {
  return(
    <div key={props.index} className="refuel-event">
      <p>{props.item.id}</p>
      <span className="refuel-event--title">Date: </span><span>{props.item.date}</span><br/>
      <span className="refuel-event--title">Kilometers: </span><span>{props.item.km}</span><br/>
      <span className="refuel-event--title">Liters: </span><span>{props.item.liters}</span><br/>
      <span className="refuel-event--title">Liter price: </span><span>{props.item.literPrice}</span><br/>
      <span className="refuel-event--title">Refuel price: </span><span>{fuelCost(props.item.liters, props.item.literPrice)}</span><br/>
      <span className="refuel-event--title">Consumption L/100km: </span><span>{consumptionPer100km(props.item.km, props.item.liters)}</span><br/><br/>
      <button onClick={() => props.onclick(props.item)} >X</button>
    </div>
  )
}

function Events() {
  const refuelingEvents = useSelector(state => state.refuelingEvents)
  const dispatch = useDispatch()
  // remove item
  const removeItem = (deleteItem) => {
    // connect database
    const db = firebase.firestore();
    // delete item from database
    db.collection('car-consumption-events').doc(deleteItem.id).delete();
    // filter/remove item with id
    const newItems = refuelingEvents.filter(item => item.id !== deleteItem.id);
    // set new items
    dispatch(addDbData(newItems))
  }
  return(
    <div>
      <ul>
        {refuelingEvents.map((item, index) => (
          <RefuelEvent key={index} item={item} onclick={removeItem}/>
        ))}
        </ul>
    </div>
  )
}

function Main() {
  const [date, setDate] = useState(null)
  const [km, setKm] = useState(0)
  const [liters, setLiters] = useState(0)
  const [literPrice, setLiterPrice] = useState(0)
  const [loading, setLoading] = useState(true)
  const dispatch = useDispatch()

  useEffect(() => {
    const fetchData = async () => {
      // database
      const db = firebase.firestore();
      // connect todos collection
      const data = await db.collection("car-consumption-events").get();
      // todo text and id (document id is unique, so it can be used here)
      const items = data.docs.map(doc => {
        return  { 
          date: doc.data().date,
          km: doc.data().km,
          liters: doc.data().liters,
          literPrice: doc.data().literPrice,
          id: doc.id 
        };
      });
      // set states
      console.log("adding items")
      dispatch(addDbData(items))
      setLoading(false)
    }
    // start loading data
    console.log("fetch data...")
    fetchData()
  },[]); // called only once
  
  // render loading... text
  if (loading) return (<p>Loading...</p>);

  const newRefuelEvent = () => {
    let obj = {
      date: date,
      km: km,
      liters: liters,
      literPrice: literPrice
    }
    return obj
  }

  const handleAdd = async () => {
    let newItem =  newRefuelEvent()
    const db = firebase.firestore()
    let doc = await db.collection('car-consumption-events').add(newItem)
    newItem.id = doc.id
    dispatch(addRefuelEvent(newItem))
  }

  return(
    <div>
      Date: <input type="date" onChange={ (event) => setDate(event.target.value)} /><br />
      Kilometers: <input type="text" onChange={ (event) => setKm(event.target.value)}/><br />
      Refueled liters: <input type="text" onChange={ (event) => setLiters(event.target.value)} /><br />
      Price per liter: <input type="text" onChange={ (event) => setLiterPrice(event.target.value)} />
      <button onClick={() => handleAdd()} >Add event</button>
    </div>
  )
}

function App() {
  const store = createStore(reducer);
  return (
    <Router>
      <Provider store={store}>
        <ul className="nav--ul">
          <li><Link to="/">Main</Link></li>
          <li><Link to="/events">Events</Link></li>
        </ul>
        <Switch>
          <Route exact path="/"> <Main /> </Route>
          <Route path="/events"> <Events /> </Route>
        </Switch>
      </Provider>
    </Router>
  );
}

export default App;
