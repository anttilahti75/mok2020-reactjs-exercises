Car consumption monitoring

Your target is to create a React application, which can monitor car fuel consumption.

Application should display a following information from each refueling event:

    date
    car kilometers (how much has been driven since the previous refueling)
    refueled liters
    price per liter
    cost (calculated, not asked)
    consumption L/100km (calculated, not asked)

and calculated total and average data from the above refueling events:

    Total kilometers driven
    Total costs
    Average consumption L/100km

Steps

    Step-1: Plan and create UI where user can give a needed data for a refueling event
    Step-2: Use React hooks to get refueling data from UI (Step-1) to React Hooks variables
    Step-3: Store data to Redux store (add a refuel event to store)
    Step-4: Plan and create UI which displays each refueling events and calculated total/averages
    Step-5: Load and show all refuel events from Redux store in the UI created in Step-4, calculate and show total/average values
    Step-6: Add a removing functionality, user can remove a selected refuel event
    Step-7: Use Routing to show (step-1 or step-4 UI)
    +Step-8: Save data somewhere, so it will be available between app launching times
    +Step-9: Include Recharts library and use it to display consumption data
    +Step-10: Include multi car support
    +Step-11: Include login/password for your app
    +Step-12: Publish your application to web server
