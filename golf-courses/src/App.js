import React, { useState, useEffect } from 'react';
import { Map, TileLayer, Marker, Popup } from 'react-leaflet';
import './Leaflet.css';
import './App.css';
import axios from 'axios';

function App() {
  const [courses, setCourses] = useState([])
  const position = [62, 26]
  const zoom = 7

  const markers = courses.map((course,index) =>
    <Marker position={[course.lat, course.lng]} key={index} >
      <Popup>
          <b>{course.course}</b><br/>
          {course.address}<br/>
          {course.phone}<br/>
          {course.email}<br/>
          <a href={course.web} target="_blank" rel="noopener noreferrer">{course.web}</a><br/>
          <br/>
          <i>{course.text}</i>
        </Popup>
    </Marker>
  );

  useEffect(() => {
    axios
      .get('golf_courses.json')
      .then(response => {
        //console.log(response.data);
        setCourses(response.data.courses);
      })
  }, [])

  return (
    <div className="App" >
      <Map center={position} zoom={zoom} style={{ height: '600px' }} >
        <TileLayer
            attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
            url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
        />
        {markers}
      </Map>
    </div>
  );
}

export default App;
